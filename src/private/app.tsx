import React from "react";
import {ValidatorProvider, ValidatorContext, ValidatorConsumer} from "../public";
import NestedExample from "./example";

export default function App() {
    return (
        <ValidatorProvider>
            <NestedExample/>
        </ValidatorProvider>
    );
}
