import {createContext} from "react";
import {IValidatorProvider} from "./provider";

// @ts-ignore
export const ValidatorContext = createContext<IValidatorProvider>({});