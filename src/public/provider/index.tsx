import React, {Component, FormEvent, ReactNode} from "react";
import {Validator} from "@nvidia1997/js-validator";
import {CALLBACK_NOT_FUNCTION} from "./constants";
import {ValidatorContext} from "../context";

interface IFormSubmitCallback {
    (e: FormEvent<HTMLFormElement>): any
}

interface IFormSubmitReturnType {
    (e: FormEvent<HTMLFormElement>): boolean
}

export interface IValidatorProvider {
    validator: Validator,
    createOnFormSubmitHandler: { (onSuccess?: IFormSubmitCallback, onError?: IFormSubmitCallback, preventDefault?: boolean): IFormSubmitReturnType },
    createOnValidationHandler: { (onSuccess: Function, onError: Function): Function },
}

interface IState {
    validator: Validator
}

export class ValidatorProvider extends Component<{ children: ReactNode }, IState> {
    updateState = (validatorId?: string) => {
        this.setState((state) => (state));
    };

    state = {
        validator: new Validator({
            onStateChanged: this.updateState,
            overrideOnIdConflict: true,
        })
    };

    isFunction = (callback: any) => typeof callback === 'function';

    /**
     * @descriptionExecutes validate method on the closest context validator and invokes the onSuccess callback if there're no errors or onError callback if there're errors. Form submission is cancelled when there're errors.
     */
    createOnFormSubmitHandler = (onSuccess?: IFormSubmitCallback, onError?: IFormSubmitCallback, preventDefault: boolean = true): IFormSubmitReturnType => {
        return (e: FormEvent<HTMLFormElement>): boolean => {
            const {validator} = this.state;
            validator.validate(false);
            let result;

            if (validator.hasErrors || preventDefault) {
                e.preventDefault();
            }

            if (!this.isFunction(onSuccess) || (onError && !this.isFunction(onError))) {
                throw new Error(CALLBACK_NOT_FUNCTION);
            }
            else if (!validator.hasErrors && onSuccess) {
                onSuccess(e);
            }
            else if (!validator.hasErrors && onError) {
                onError(e);
                result = false;
            }
            else {
                result = false;
            }

            validator.onStateChanged();

            return typeof result !== 'undefined' ? result : !preventDefault;
        };
    };

    /**
     * @description Executes validate method on the closest context validator and invokes the onSuccess callback if there're no errors or onError callback if there're errors.
     */
    createOnValidationHandler = (onSuccess: Function, onError: Function): Function => {
        return (...params: any[]): any => {
            const {validator} = this.state;
            validator.validate(false);
            let result;

            if (!this.isFunction(onSuccess) || (onError && !this.isFunction(onError))) {
                throw new Error(CALLBACK_NOT_FUNCTION);
            }
            else if (!validator.hasErrors && onSuccess) {
                result = onSuccess(...params);
            }
            else if (validator.hasErrors && onError) {
                result = onError(...params);
            }

            validator.onStateChanged();

            return result;
        };
    };

    render() {
        const {children} = this.props;

        return (
            <ValidatorContext.Provider
                value={{
                    ...this.state,
                    createOnFormSubmitHandler: this.createOnFormSubmitHandler,
                    createOnValidationHandler: this.createOnValidationHandler,
                }}
            >
                {children}
            </ValidatorContext.Provider>
        );
    }
}
