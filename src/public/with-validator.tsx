import React from "react";
import {ValidatorProvider} from "./provider";

export const withValidator = (PassedComponent: (...params: any[]) => any) => {
    return (props: object) => {
        return (
            <ValidatorProvider>
                <PassedComponent {...props}/>
            </ValidatorProvider>
        );
    };
};
