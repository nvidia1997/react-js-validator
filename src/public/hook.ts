import {useContext} from "react";
import {ValidatorContext} from "./context";
import {IValidatorProvider} from "./provider";

export function useValidatorContext(): IValidatorProvider {
    return useContext(ValidatorContext);
}