# @nvidia1997/react-js-validator

#### This is react-wrapper for [js-validator](https://www.npmjs.com/package/@nvidia1997/js-validator).
#### Please read its docs first!

## How to use:
### Import context related stuff 
```javascript
import {ValidatorProvider, ValidatorContext, ValidatorConsumer, withValidator} from "@nvidia1997/react-js-validator";
```

#### Inject the validator context

#### Way 1 (done in the parent component)
```javascript
<SomeComponent> // not necessary to wrap ValidatorProvider
        <ValidatorProvider>// in this case we only need to import ValidatorProvider
            <WrappedComponent/>
        </ValidatorProvider>
</SomeComponent>
```

#### Way 2 (done in the target component)
```javascript
function WrappedComponent(props){
  return (
    <div>some text</div>
  );
}

export default withValidator(WrappedComponent);
```

#### In WrappedComponent

#### Imports
```jsx
import {useValidatorContext} from "@nvidia1997/react-js-validator";
```

#### Initializing
```jsx
     const [text1, setText1] = useState("");
     const [text2, setText2] = useState("");

    // timestamp variable is needed only if you're using allowNull, allowUndefined, notRequired validations and validation on change at the same time
     const [timestamp, setTimestamp] = useState(Date.now());

     const {
         validator,
         createOnFormSubmitHandler,
         createOnValidationSuccessHandler
     } = useValidatorContext();
```

#### Declaring sub validators
```jsx
 useEffect(() => {
        validator
            .string(text1, "text1Validator")
            .notNull() 
            .notUndefined() 
            .maxLength(2)
            //.validate(false); // uncomment if you want the validations to occur on text change

        validator
            .string(text2, "text2Validator")
            .maxLength(3)
            //.validate(); //only last validate method must be without "false" as param. This will fire onStateChanged only once.
    
      return () => {
          validator.removeValidator("text1Validator");//remove validators when dismounting components to avoid hidden validation errors
          validator.removeValidator("text2Validator");
        }
}, [validator, text1, text2, timestamp]);// if we're not doing validation on change, timestamp is not required
```



#### Form validation
```jsx
 const onSubmitSuccess = (e) => {
        console.log("form submitted");
    };

 const onSubmitFailure = (e) => {
        console.log("form NOT submitted, validation errors occurred");
    };

 const onText1Change = (e) => {
        setText1(e.target.value);
    };

    const onText2Change = (e) => {
        setText2(e.target.value);
    };

<form onSubmit={createOnFormSubmitHandler(onSubmitSuccess, onSubmitFailure)} action="/some_action.php">
      <input type="text" value={text1} onChange={onText1Change}/>
      <input type="text" value={text2} onChange={onText2Change}/>
      <input type="submit" value="Submit"/>
</form>
```

#### Custom validation
```jsx
 const onValidationSuccess = (e) => {
         console.log("validation successful");
     }; 

const onValidationFailure= (e) => {
         console.log("oops, some fields have errors");
     };

 <button onClick={createOnValidationHandler(onValidationSuccess, onValidationFailure)}>
   validate
 </button>
```

#### Reset errors
```jsx
 <button onClick={() => {validator.reset(); }}>
     reset
</button>

   <span>{validator.hasErrors.toString()}</span>
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://gitlab.com/nvidia1997/js-validator/LICENSE)
